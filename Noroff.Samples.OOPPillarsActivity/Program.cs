﻿namespace Noroff.Samples.OOPPillarsActivity
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Create an orbiter with a specific minimum altitude
            Orbiter orbiter = new Orbiter(1200);
            orbiter.FuelLevel = 100; // Setting initial fuel level
            orbiter.Launch();
            orbiter.EnterOrbit(orbiter.MinimumAltitude);
            orbiter.CollectData();
            orbiter.TransmitData();
            orbiter.Land();

            Console.WriteLine();

            // Create a rover
            Rover rover = new Rover();
            rover.FuelLevel = 80; // Setting initial fuel level
            rover.Launch();
            rover.Land();
            rover.Explore(20); // Exploring a distance of 20 km
            rover.CollectData();
            rover.TransmitData();

            Console.WriteLine();

            // Create a space probe with a destination
            SpaceProbe spaceProbe = new SpaceProbe { Destination = "Mars" };
            spaceProbe.FuelLevel = 150; // Setting initial fuel level
            spaceProbe.Launch();
            spaceProbe.CollectData();
            spaceProbe.TransmitData();
            spaceProbe.Land();
        }

        public abstract class Spacecraft
        {
            public double FuelLevel { get; set; }
            public string ScientificData { get; set; }
            public TimeSpan MissionDuration { get; set; }

            public abstract void Launch();
            public abstract void Land();

            public virtual void CollectData()
            {
                ScientificData = $"Generic Space Data collected at {DateTime.UtcNow}";
            }

            public virtual void TransmitData()
            {
                Console.WriteLine($"Transmitting data: {ScientificData}");
            }

        }

        public class Orbiter : Spacecraft
        {
            public double MinimumAltitude { get; } = 1000;

            public Orbiter()
            {
            }

            public Orbiter(double minimumAltitude)
            {
                MinimumAltitude = minimumAltitude;
            }

            public override void TransmitData()
            {
                Console.WriteLine($"Orbiter transmitting orbital data: {ScientificData}");
            }

            public void EnterOrbit(double altitude)
            {
                Console.WriteLine($"Orbiter has entered orbit at altitude {altitude} km.");
            }

            public override void Launch()
            {
                FuelLevel -= 50; // Consuming fuel
                Console.WriteLine("Orbiter launched successfully.");
            }

            public override void Land()
            {
                Console.WriteLine("Orbiter landing sequence initiated.");
            }

        }

        public class Rover : Spacecraft
        {
            public override void TransmitData()
            {
                Console.WriteLine($"Rover transmitting surface data: {ScientificData}");
            }

            public void Explore(double distance)
            {
                if (FuelLevel < distance / 2) // Assuming fuel consumption rate
                {
                    Console.WriteLine("Insufficient fuel to complete the exploration.");
                    return;
                }

                FuelLevel -= distance / 2; // Consuming fuel
                Console.WriteLine($"Rover explored {distance} km.");
            }

            public override void Launch()
            {
                FuelLevel -= 30; // Consuming less fuel than orbiter
                Console.WriteLine("Rover launched successfully.");
            }

            public override void Land()
            {
                Console.WriteLine("Rover has landed on the surface.");
            }
        }

        public class SpaceProbe : Spacecraft
        {
            public string Destination { get; set; }

            public override void Launch()
            {
                FuelLevel -= 70; // High fuel consumption for deep space
                Console.WriteLine($"SpaceProbe launched towards {Destination}.");
            }

            public override void Land()
            {
                // Probes usually don't land but let's assume this one can.
                Console.WriteLine($"SpaceProbe landing initiated for {Destination}.");
            }

            public override void CollectData()
            {
                ScientificData = $"Data from {Destination}: [REDACTED]";
            }

            public override void TransmitData()
            {
                Console.WriteLine($"SpaceProbe transmitting data from {Destination}: {ScientificData}");
            }
        }
    }
}