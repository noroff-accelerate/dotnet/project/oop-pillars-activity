# Space Exploration Simulation Activity

## Overview
This activity involves creating a space exploration simulation using Object-Oriented Programming (OOP) principles in C#. The simulation will include different types of spacecraft: Orbiter, Rover, and SpaceProbe, each with unique behaviors and functionalities.

## Setup
- Create a new console application in your preferred C# IDE.

## Implementation

### Spacecraft Base Class
- Define an abstract class `Spacecraft` with the following members:
  - Properties: `FuelLevel`, `ScientificData`, `MissionDuration`.
  - Abstract methods: `Launch()`, `Land()`.
  - Virtual methods: `CollectData()`, `TransmitData()`.

### Orbiter Subclass
- Create a subclass of `Spacecraft` named `Orbiter`.
- Override `TransmitData()`, `Launch()`, and `Land()` methods.
- Add a method `EnterOrbit(double altitude)`.

### Rover Subclass
- Implement the `Rover` subclass of `Spacecraft`.
- Override `TransmitData()`, `Launch()`, and `Land()` methods.
- Add `Explore(double distance)` for planetary surface exploration.

### SpaceProbe Class
- Introduce the `SpaceProbe` class, inheriting from `Spacecraft`.
- Include a property `Destination`.
- Override `CollectData()`, `TransmitData()`, `Launch()`, and `Land()`.

## Simulation in Main Method
- Instantiate each type of spacecraft and simulate a complete mission cycle.
- Perform specific tasks for each spacecraft:
  - `Orbiter`: Enter orbit and transmit data.
  - `Rover`: Explore and transmit data.
  - `SpaceProbe`: Collect and transmit data from the destination.

## Reflection and Discussion
- After the simulation, discuss how OOP principles like abstraction, encapsulation, inheritance, and polymorphism were utilized.
- Reflect on how the spacecraft classes could be extended or modified for different types of space missions.

## Conclusion
This activity provides hands-on experience in applying key OOP concepts in a creative and engaging context, enhancing understanding and skills in software development.
